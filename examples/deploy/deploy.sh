#!/bin/sh

cat <<USAGE

This script will create an index.html file in the public/ dir.
This will create a little site when used with GitLab CI and GitLab pages.

USAGE

[ "$CI" ] || { echo "Only deploy using CI" 1>&2; exit 1; }

project_dir="$(dirname $0)/../.."
gitlab_pages_dir="$project_dir/public"
index_html="$gitlab_pages_dir/index.html"

[ -d "$gitlab_pages_dir" ] || mkdir -p "$gitlab_pages_dir"

cat <<INDEX_HTML > "${index_html}"
<!doctype html>
<html>
  <head>
    <title>CI deploy test</title>
  </head>
  <body>
    <h1>Deployment</h1>
    <p>
      Congratulations, ${GITLAB_USER_NAME}!
      You're a deployer!
    </p>
  </body>
</html>
INDEX_HTML
